import json
import requests
import datetime
import boto3
import time
from decimal import Decimal


PATH_TO_JSON = "../config/users.json"

class WeatherClient():

    def __init__(self, latitude, longitude, device_type, farm_name, farm_owner):
        AWS_REGION = 'us-east-1'
        self.api_key = "6cfdeb62bc5d2e6c9c86086b87221f3a"
        # Somewhere near Hyderabad
        self.latitude = latitude
        self.longitude =  longitude 
        self.device_type = device_type
        self.farm_name = farm_name
        self.farm_owner = farm_owner
        dynamodb_res = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.weather_table = dynamodb_res.Table('g5_weatherdata')

    def update_weather_data(self):
        query = {'lat' : self.latitude, 
                'lon': self.longitude,
                'appid': self.api_key ,
                'units': 'metric'
                }
        json_response = requests.get('https://api.openweathermap.org/data/2.5/weather', params=query).json()
        message = {
            'timestamp': str(datetime.datetime.now()),
            'temperature': json_response["main"]["temp"],
            'humidity' : json_response["main"]["humidity"],
            'device_type': self.device_type,
            'lat': self.latitude,
            'long': self.longitude,
            'farm_name': self.farm_name,
            "farm_owner": self.farm_owner
        }
        messageJson = json.loads(json.dumps(message), parse_float=Decimal)
        print(messageJson)
        response = self.weather_table.put_item(Item=messageJson)

if __name__ == '__main__':
    weather_clients = []

    with open(PATH_TO_JSON, "r") as file:
        data = json.load(file)
        for (user, user_data) in data.items():
            farm = user_data["farm"]
            for thing in farm["iot_things"]:
                weather_clients.append(WeatherClient(thing["lat"], thing["long"], thing["device_type"], farm["farm_name"], user))

    #push data interval of 5 mins
    while (True):

        # (TODO) Periodically publish weather data
        for client in weather_clients:
            client.update_weather_data()

            time.sleep(5)