import time
import json
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT
import random
import datetime
import time
import sched

# Define ENDPOINT, TOPIC, RELATOVE DIRECTORY for CERTIFICATE AND KEYS
ENDPOINT = "a2tf5mpgtt6bd5-ats.iot.us-east-1.amazonaws.com"
PATH_TO_CERT = "../config"
PATH_TO_JSON = "../config/users.json"
TOPIC = "iot/agritech"
TOPIC_SPRINKLER = "iot/agritech/username/farmname/sprkid"


# callback function for sprinker event
def sprinkler_status_event(client, userdata, message):
    print('>>>>>>>>>>>>>>>>>>>>>>>>>>>   Received status event', message)
    print(f"Received `{message.payload.decode()}` from `{message.topic}` topic")


# AWS class to create number of objects (devices)
class AWS():
    # Constructor that accepts client id that works as device id and file names for different devices
    # This method will obviosuly be called while creating the instance
    # It will create the MQTT client for AWS using the credentials
    # Connect operation will make sure that connection is established between the device and AWS MQTT
    def __init__(self, client, certificate, private_key, measurement_type):
        self.client_id = client
        self.device_id = client
        self.measurement_type = measurement_type
        self.cert_path = PATH_TO_CERT + "/" + certificate
        self.pvt_key_path = PATH_TO_CERT + "/" + private_key
        self.root_path = PATH_TO_CERT + "/" + "group5RootCA1.pem"
        self.myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(self.client_id)
        self.myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
        self.myAWSIoTMQTTClient.configureCredentials(self.root_path, self.pvt_key_path, self.cert_path)
        self._connect()

    # Connect method to establish connection with AWS IoT core MQTT
    def _connect(self):
        self.myAWSIoTMQTTClient.connect()

    # This method will publish the data on MQTT
    # Before publishing we are confiuguring message to be published on MQTT
    def publish(self, value):
        print('Begin Publish')
        message = {}
        timestamp = str(datetime.datetime.now())
        message['deviceid'] = self.device_id
        message['timestamp'] = timestamp
        message['value'] = value
        message['device_type'] = self.measurement_type
        messageJson = json.dumps(message)
        self.myAWSIoTMQTTClient.publish(TOPIC, messageJson, 1)
        print("Published: '" + json.dumps(message) + "' to the topic: " + TOPIC)
        time.sleep(0.1)
        print('Publish End')

    # This method will publish the data on MQTT
    # Before publishing we are configuring message to be published on MQTT
    def publish_sprinkler(self, sprk_id):
        print('Begin Publish')
        message = {}
        timestamp = str(datetime.datetime.now())
        soil_temp = temperature
        soil_moist = moisture
        print("check the device ID *******", sprk_id)
        message['timestamp'] = timestamp
        #message['spkrid'] = self.device_id
        message[sprk_id + 'ss_avg'] = 30
        message[sprk_id + 'sm_avg'] = 40
        message[sprk_id + 'weather_temp'] = 50
        message[sprk_id + 'weather_moisture'] = 60
        print("ST", soil_temp)
        print("SM", soil_moist)
    # Sprinkler switching Logic
        if (soil_temp < 25):
            if (soil_moist > 80):
                message[sprk_id] = "OFF"
        else:
            message[sprk_id] = "ON"

        messageJson = json.dumps(message)
        self.myAWSIoTMQTTClient.publish(TOPIC_SPRINKLER, messageJson, 1)
        print("Published: '" + json.dumps(message) + "' to the topic: " + TOPIC_SPRINKLER)
        time.sleep(0.1)
        print('Publish End')

    # This method will publish the data on MQTT
    # Before publishing we are confiuguring message to be published on MQTT
    def subscribe_sprinkler(self):
        print('Begin Subscribe')
        self.myAWSIoTMQTTClient.subscribe(TOPIC_SPRINKLER, 0, sprinkler_status_event)
        print('Subscribe End')
        print('sprinkler_status: ', TOPIC_SPRINKLER)
        time.sleep(0.1)

    # Disconnect operation for each devices
    def disconnect(self):
        self.myAWSIoTMQTTClient.disconnect()


# Main method with actual objects and method calling to publish the data in MQTT
# Again this is a minimal example that can be extended to incorporate more devices
# Also there can be different method calls as well based on the devices and their working.
if __name__ == '__main__':
    # Spil sensor device Objects

    temperature_sensors = []
    moisture_sensors = []
    with open(PATH_TO_JSON, "r") as file:
        data = json.load(file)
        for (user, user_data) in data.items():
            farm = user_data["farm"]
            for thing in farm["iot_things"]:
                if thing["device_type"] == "soil_temperature":
                    for device in thing["devices"]:
                        device_id = f"agritech_{user}_{farm['farm_name']}_{device}"
                        print(device_id)
                        temperature_sensors.append(
                            AWS(device_id, f"{device_id}.crt", f"{device_id}.key", "soil_temperature"))
                if thing["device_type"] == "soil_moisture":
                    for device in thing["devices"]:
                        device_id = f"agritech_{user}_{farm['farm_name']}_{device}"
                        moisture_sensors.append(AWS(device_id, f"{device_id}.crt", f"{device_id}.key", "soil_moisture"))

        # push data interval of 5 mins
        # while (True):


        # (TODO) Periodically Publish this data
        for temperature_sensor in temperature_sensors:
            temperature = round(float(random.normalvariate(25, 3)), 1)
            temperature_sensor.publish(temperature)
            print('Publish sprinkler')
            #temperature_sensor.publish_sprinkler()

        for moisture_sensor in moisture_sensors:
            moisture = round(float(random.normalvariate(80, 10)), 1)
            moisture_sensor.publish(moisture)

    file.close()

# Sprinkler publish code
    print("-----Start of Sprinkler Code -------")
    sprinklers_data = []
    with open(PATH_TO_JSON, "r") as file1:
        data = json.load(file1)
        for (user, user_data) in data.items():
            farm = user_data["farm"]
            for thing in farm["iot_things"]:
                if thing["device_type"] == "sprinkler":
                    for device in thing["devices"]:
                        #device_id = f"agritech_{user}_{farm['farm_name']}_{device}"
                        sprk_id = f"agritech_{user}_{farm['farm_name']}_sprk001"
                        print(sprk_id)
                        #sprinklers_data.append(
                        #    AWS(device_id, f"{device_id}.crt", f"{device_id}.key", "sprinkler"))
                        sprk_obj = AWS(sprk_id, f"{sprk_id}.crt", f"{sprk_id}.key", "sprinkler")
                        sprinklers_data.append(sprk_obj)
                        sprk_obj.subscribe_sprinkler()
                        sprk_obj.publish_sprinkler(sprk_id)

        # push data interval of 5 mins
        # while (True):

        #sprinklers = AWS('agritech_abhishek_farm01_sprk001', '../config/agritech_abhishek_farm01_sprk001.crt',
        #                 '../config/agritech_abhishek_farm01_sprk001.key', "soil_sprinkler")
        #sprinklers.subscribe_sprinkler()

        # (TODO) Periodically Publish this data

        #for sprinklers in sprinklers_data:
        #    print('Publish sprinkler')
        #    sprinklers.publish_sprinkler()

        file1.close()

        # time.sleep(5)

    # print('Publish sprinkler')
    # AWS('agritech_abhishek_farm01_sm001', '../config/agritech_abhishek_farm01_sprk001.crt', '../config/agritech_abhishek_farm01_sm001.key',"soil_temperature").publish_sprinkler()

