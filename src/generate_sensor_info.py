
import json

sensor_types = [
    ("ss", "soil_temperature"), 
    ("sm", "soil_moisture")
    ]
sprinkler_types = [
    ("sprk", "sprinkler")
    ]
names = [
    {"username": "arpit", "usertype": "owner"}, 
    {"username": "kapilesh", "usertype": "owner"}, 
    {"username": "abhishek", "usertype": "owner"}, 
    {"username": "arokya", "usertype": "owner"},
    {"username": "mallik", "usertype": "owner"}
   # {"username": "admin", "usertype": "admin"}
    ]
data = {}


def createFarm(farmName, sensorCount, sprinklerCount, lat, long):
    farm = {
        "farm_name": farmName,
        "iot_things": []
    }
    for (code, type) in sensor_types:
        deviceType = {
            "device_type": type,
            "lat": lat,
            "long": long,
            "devices": [f"{code}00{i}" for i in range(1, sensorCount + 1)]
        }
        farm["iot_things"].append(deviceType)

    for (code, type) in sprinkler_types:
        deviceType = {
            "device_type": type,
            "lat": lat,
            "long": long,
            "devices": []
        }
        for i in range(1, sprinklerCount):
            tempDevices = {
             "id": f"{code}00{i}",
             "capacity_in_lts_per_hour": 10,
             "soil_temp_threshold": 50,
             "soil_humidity_threshold": 60,
             "air_temp_threshold": 55,
             "air_humidity_threshold": 65,
            }
            deviceType['devices'].append(tempDevices)
        farm["iot_things"].append(deviceType)
    return farm

for name in names:
    userData = {
        "username": name['username'],
        "password": name['username'],
        "role": name['usertype'],
        "farm": createFarm("farm01", 2, 2, 12.9716, 77.5946)
    }
    data[name['username']] = userData

with open("../config/users.json", "w") as outfile:
    outfile.write(json.dumps(data, indent=4))
